package openyaml.vadim99808.type;

public enum PathType {

    MANY_PER_FILE,
    ONE_PER_FILE

}
