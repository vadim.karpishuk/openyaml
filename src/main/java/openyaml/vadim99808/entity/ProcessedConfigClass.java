package openyaml.vadim99808.entity;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

public class ProcessedConfigClass {

    private Class representedClass;
    private Map<Field, List<Annotation>> fieldAnnotationMap;
    private ProcessedConfigClass parent;

    public Map<Field, List<Annotation>> getFieldAnnotationMap() {
        return fieldAnnotationMap;
    }

    public void setFieldAnnotationMap(Map<Field, List<Annotation>> fieldAnnotationMap) {
        this.fieldAnnotationMap = fieldAnnotationMap;
    }

    public ProcessedConfigClass getParent() {
        return parent;
    }

    public void setParent(ProcessedConfigClass parent) {
        this.parent = parent;
    }

    public Class getRepresentedClass() {
        return representedClass;
    }

    public void setRepresentedClass(Class representedClass) {
        this.representedClass = representedClass;
    }
}
