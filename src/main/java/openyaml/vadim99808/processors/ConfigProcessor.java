package openyaml.vadim99808.processors;

import openyaml.vadim99808.deprecated.annotations.ConfigPath;
import openyaml.vadim99808.entity.ProcessedConfigClass;
import openyaml.vadim99808.openYaml;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.*;

public class ConfigProcessor {

    private static ConfigProcessor instance;

    private ConfigProcessor(){}

    public static ConfigProcessor getInstance(){
        if(instance == null){
            instance = new ConfigProcessor();
        }
        return instance;
    }

    public Object process(Class classToProcess, Object childConfig){
        Map.Entry<File, Annotation> entry;
        if(childConfig == null) {
            entry = processPath(classToProcess);
        }else{
            entry = processPath(childConfig.getClass());
        }

        if(entry.getKey().getName().endsWith(".yml")){
            return processAsSingleConfig()
        }

        processAsMultipleConfigs(entry.getKey(), entry.getValue(), classToProcess, childConfig);
    }

    private Map.Entry<File, Annotation> processPath(Class classToProcess){
        ProcessedConfigClass processedConfigClass = ConfigClassProcessor.getInstance().process(classToProcess);
        Object config = createInstance(classToProcess);
        if(config == null){
            return null;
        }

        Map<File, Annotation> path = definePath(processedConfigClass, config);
        if(path.isEmpty()){
            openYaml.getInstance().getLogger().warning("Cannot find @ConfigPath annotation to get path to configs in class " + classToProcess.getName());
        }
        Set<Map.Entry<File, Annotation>> entrySet = path.entrySet();
        return entrySet.stream().findFirst().get();
    }

    private Object processAsSingleConfig(File file, Object){}

    private Object processAsMultipleConfigs(File file, ConfigPath configPath, ProcessedConfigClass processedConfigClass, Object childConfig){
        Object object;
        if(childConfig != null){
            object = childConfig;
        }else{
            object = createInstance(processedConfigClass.getRepresentedClass());
        }
    }

    private Object createInstance(Class classToCreateInstance){
        Object config;
        try {
            config = classToCreateInstance.newInstance();
        } catch (InstantiationException e) {
            openYaml.getInstance().getLogger().warning("Error with trying to instantiate class " + classToCreateInstance.getName());
            return null;
        } catch (IllegalAccessException e) {
            openYaml.getInstance().getLogger().warning("There are no public constructors in class " + classToCreateInstance.getName());
            return null;
        }
        return config;
    }

    private Map<File, Annotation> definePath(ProcessedConfigClass processedConfigClass, Object config){
        Set<Map.Entry<Field, List<Annotation>>> entries = processedConfigClass.getFieldAnnotationMap().entrySet();
        Map<File, Annotation> fileAnnotationHashMap = new HashMap<>();
        Field field = null;
        Optional<Annotation> annotation = Optional.empty();
        for(Map.Entry<Field, List<Annotation>> entry: entries){
            annotation = entry.getValue().stream().filter(localAnnotation -> localAnnotation instanceof ConfigPath).findFirst();
            if(annotation.isPresent()){
                field = entry.getKey();
                break;
            }else{
                return fileAnnotationHashMap;
            }
        }
        fileAnnotationHashMap.put((File)FieldClassProcessor.getInstance().getFieldValue(field, config), annotation.get());
        return fileAnnotationHashMap;
    }

}
