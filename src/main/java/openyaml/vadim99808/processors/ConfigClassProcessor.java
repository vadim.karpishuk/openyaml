package openyaml.vadim99808.processors;

import openyaml.vadim99808.entity.ProcessedConfigClass;

public class ConfigClassProcessor {

    private static ConfigClassProcessor instance;

    private ConfigClassProcessor(){}

    public static ConfigClassProcessor getInstance() {
        if(instance == null){
            instance = new ConfigClassProcessor();
        }
        return instance;
    }

    public ProcessedConfigClass process(Class classToProcess){
        ProcessedConfigClass processedConfigClass = new ProcessedConfigClass();
        processedConfigClass.setRepresentedClass(classToProcess);
        processedConfigClass.setFieldAnnotationMap(FieldClassProcessor.getInstance().process(classToProcess));
        processedConfigClass.setParent(ParentClassProcessor.getInstance().process(classToProcess));
        return processedConfigClass;
    }
}
