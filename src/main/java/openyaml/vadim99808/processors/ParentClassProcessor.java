package openyaml.vadim99808.processors;

import openyaml.vadim99808.entity.ProcessedConfigClass;

import java.util.List;

public class ParentClassProcessor {

    private static ParentClassProcessor instance;

    private ParentClassProcessor(){}

    public static ParentClassProcessor getInstance(){
        if(instance == null){
            instance = new ParentClassProcessor();
        }
        return instance;
    }

    public ProcessedConfigClass process(Class classToProcess){
        Class parentClass = classToProcess.getSuperclass();
        if(!parentClass.equals(Object.class)){
           return ConfigClassProcessor.getInstance().process(parentClass);
        }
        return null;
    }

}
