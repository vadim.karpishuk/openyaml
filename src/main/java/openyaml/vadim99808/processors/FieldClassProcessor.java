package openyaml.vadim99808.processors;

import openyaml.vadim99808.deprecated.annotations.ConfigChild;
import openyaml.vadim99808.deprecated.annotations.ConfigField;
import openyaml.vadim99808.deprecated.annotations.ConfigPath;
import openyaml.vadim99808.deprecated.annotations.Primary;
import openyaml.vadim99808.openYaml;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

public class FieldClassProcessor {

    private static FieldClassProcessor instance;

    private FieldClassProcessor(){}

    public static FieldClassProcessor getInstance(){
        if(instance == null){
            instance = new FieldClassProcessor();
        }
        return instance;
    }

    public Map<Field, List<Annotation>> process(Class classToProcess){
        Field[] fields = classToProcess.getDeclaredFields();
        Map<Field, List<Annotation>> fieldAnnotationsMap = new HashMap<>();
        for(Field field: fields){
            List<Annotation> annotationList = processAnnotations(field);
            if(!annotationList.isEmpty()) {
                fieldAnnotationsMap.put(field, processAnnotations(field));
            }
        }
        return fieldAnnotationsMap;
    }

    private List<Annotation> processAnnotations(Field field){
        Annotation[] annotations = field.getDeclaredAnnotations();
        List<Annotation> annotationList = new ArrayList<>(Arrays.asList(annotations));
        return annotationList.stream().filter(annotation -> annotation instanceof ConfigPath
                || annotation instanceof ConfigField
                || annotation instanceof ConfigChild
                || annotation instanceof Primary).collect(Collectors.toList());
    }

    public Object getFieldValue(Field field, Object config){
        field.setAccessible(true);
        Object object;
        try {
            object = field.get(config);
        } catch (IllegalAccessException e) {
            openYaml.getInstance().getLogger().warning("Cannot access to field " + field.getName() + " of class " + config.getClass().getName());
            field.setAccessible(false);
            return null;
        }
        field.setAccessible(false);
        return object;
    }

}
