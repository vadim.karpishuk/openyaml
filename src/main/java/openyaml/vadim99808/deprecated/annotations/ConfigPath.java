package openyaml.vadim99808.deprecated.annotations;

import openyaml.vadim99808.type.PathType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ConfigPath {
    PathType pathType() default PathType.ONE_PER_FILE;
}
