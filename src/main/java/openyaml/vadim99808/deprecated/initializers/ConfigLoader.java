package openyaml.vadim99808.deprecated.initializers;

import openyaml.vadim99808.deprecated.handlers.ConfigHandler;

public class ConfigLoader<T> {

    private ConfigHandler configHandler;

    public ConfigLoader(){
        configHandler = new ConfigHandler();
    }

    public Object loadConfig(Class<T> tClass){
        return configHandler.handle(tClass);
    }
}
