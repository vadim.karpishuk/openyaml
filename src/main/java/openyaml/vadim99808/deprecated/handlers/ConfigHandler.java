package openyaml.vadim99808.deprecated.handlers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ConfigHandler {

    private FileHandler fileHandler;
    private FieldHandler fieldHandler;

    public ConfigHandler(){
        fileHandler = new FileHandler();
        fieldHandler = new FieldHandler();
    }

    public Object handle(Class configClass){
        Object object = createNewInstance(configClass);
        File file = fileHandler.getPath(object);
        if(file == null){
            System.out.println("[openYaml] Cannot find @ConfigPath annotation to read config of class " + object.getClass().getName() + ".");
            return null;
        }
        if(!file.exists()){
            if(file.getName().endsWith(".yml")) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    System.out.println("[openYaml] Cannot create new file of configuration of class " + configClass.getName());
                    return null;
                }
            }else{
                file.mkdir();
            }
        }
        if(!file.isDirectory()){
            return fieldHandler.handle(object, file);
        }else{
            File[] files = file.listFiles();
            List<File> fileList = Arrays.stream(files).filter(file1 -> file1.getName().endsWith(".yml")).collect(Collectors.toList());
            List<Object> objectList = new ArrayList<>();
            for(File fileCollection: fileList){
                Object handledConfig = fieldHandler.handle(object, fileCollection);
                if(handledConfig != null) {
                    objectList.add(fieldHandler.handle(object, fileCollection));
                }
                object = createNewInstance(configClass);
            }
            return objectList;
        }
    }

    private Object createNewInstance(Class configClass){
        try {
            return configClass.newInstance();
        } catch (InstantiationException e) {
            System.out.println("[openYaml] Cannot instantiate class " + configClass.getName() + ".");
        } catch (IllegalAccessException e) {
            System.out.println("[openYaml] Cannot instantiate class " + configClass.getName() + " due to inaccessible constructor.");
        }
        return null;
    }

}
