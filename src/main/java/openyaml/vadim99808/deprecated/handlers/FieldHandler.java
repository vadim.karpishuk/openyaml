package openyaml.vadim99808.deprecated.handlers;

import openyaml.vadim99808.deprecated.annotations.ConfigChild;
import openyaml.vadim99808.deprecated.annotations.ConfigField;
import openyaml.vadim99808.deprecated.annotations.Primary;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FieldHandler {

    private AnnotationHandler annotationHandler;

    public FieldHandler(){
        annotationHandler = new AnnotationHandler();
    }

    public Object handle(Object object, File file){
        List<Field> fieldList = getFields(object);
        for(Field field: fieldList){
            fillFieldConfig(object, field, file);
            if(!validField(field, object)){
                System.out.println("[openYaml] Config " + object.getClass() + " in file " + file.getName() + " is not valid and cannot be loaded. Field with name " + field.getName() + " is empty!");
                return null;
            }
        }
        return object;
    }

    private boolean validField(Field field, Object configObject){
        if(!Optional.class.isAssignableFrom(field.getType())){
            Object object;
            try {
                field.setAccessible(true);
                object = field.get(configObject);
                field.setAccessible(false);
            } catch (IllegalAccessException e) {
                System.out.println("[openYaml] Cannot access(set) to field " + field.getName() + " in config class " + configObject.getClass().getName() + ".");
                field.setAccessible(false);
                return false;
            }
            return object != null;
        }
        return true;
    }

    private List<Field> getFields(Object object){
        Field[] fields = object.getClass().getDeclaredFields();
        return new ArrayList<>(Arrays.asList(fields));
    }

    private List<Field> getFields(Class customClass){
        Field[] fields = customClass.getDeclaredFields();
        return new ArrayList<>(Arrays.asList(fields));
    }

    private void fillFieldConfig(Object configObject, Field fieldToFill, File configFile){
        List<Annotation> annotationList = annotationHandler.getAnnotations(fieldToFill);
        Optional<Annotation> optionalAnnotationConfigField = annotationHandler.getAnnotationFromList(annotationList, ConfigField.class);
        if(!optionalAnnotationConfigField.isPresent()){
            return;
        }
        ConfigField configField = (ConfigField) optionalAnnotationConfigField.get();
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(configFile);
        Optional<Annotation> optionalAnnotationConfigChild = annotationHandler.getAnnotationFromList(annotationList, ConfigChild.class);
        if(optionalAnnotationConfigChild.isPresent()){
            Class childClass = defineRootClass(fieldToFill);
            if(isCollectionTypeField(fieldToFill)){
                List<?> primariesList = fileConfiguration.getStringList(configField.name());
                if(Optional.class.isAssignableFrom(fieldToFill.getType())){
                    fillFieldWithValue(configObject, fieldToFill, wrapIntoOptional(getChildObjectsByPrimariesList(childClass, primariesList)));
                }else{
                    fillFieldWithValue(configObject, fieldToFill, getChildObjectsByPrimariesList(childClass, primariesList));
                }
            }else{
                Object primaryObject = fileConfiguration.get(configField.name());
                if(Optional.class.isAssignableFrom(fieldToFill.getType())) {
                    fillFieldWithValue(configObject, fieldToFill, wrapIntoOptional(getChildObjectByPrimary(childClass, primaryObject)));
                }else{
                    fillFieldWithValue(configObject, fieldToFill, getChildObjectByPrimary(childClass, primaryObject));
                }
            }
        }else{
            Class childClass = defineRootClass(fieldToFill);
            if(childClass.isEnum()){
                Object[] objects = childClass.getEnumConstants();
                for(Object object: objects){
                    String string = object.toString();
                    if(string.equalsIgnoreCase(fileConfiguration.getString(configField.name()))){
                        fillFieldWithValue(configObject, fieldToFill, object);
                        return;
                    }
                }
                System.out.println("[openYaml] Cannot find enum type for field " + configField.name());
                return;
            }
            if(Optional.class.isAssignableFrom(fieldToFill.getType())) {
                if(!hasMapType(fieldToFill)) {
                    fillFieldWithValue(configObject, fieldToFill, wrapIntoOptional(fileConfiguration.get(configField.name())));
                }else{
                    if(fileConfiguration.contains(configField.name())) {
                        fillFieldWithValue(configObject, fieldToFill, wrapIntoOptional(fileConfiguration.getConfigurationSection(configField.name()).getValues(false)));
                    }else{
                        fillFieldWithValue(configObject, fieldToFill, wrapIntoOptional(null));
                    }
                }
            }else{
                if(!Map.class.isAssignableFrom(fieldToFill.getType())) {
                    fillFieldWithValue(configObject, fieldToFill, fileConfiguration.get(configField.name()));
                }else{
                    if(fileConfiguration.contains(configField.name())) {
                        fillFieldWithValue(configObject, fieldToFill, fileConfiguration.getConfigurationSection(configField.name()).getValues(false));
                    }else{
                        fillFieldWithValue(configObject, fieldToFill, wrapIntoOptional(null));
                    }
                }
            }
        }
    }

    private boolean hasMapType(Field field){
        Class currentClassToRoot = field.getType();
        if(Optional.class.isAssignableFrom(currentClassToRoot)){
            ParameterizedType parameterizedType = (ParameterizedType) field.getGenericType();
            Type type = parameterizedType.getActualTypeArguments()[0];
            if(type instanceof ParameterizedTypeImpl){
                currentClassToRoot = ((ParameterizedTypeImpl) type).getRawType();
            }else{
                currentClassToRoot = (Class) type;
            }
        }
        return Map.class.isAssignableFrom(currentClassToRoot);
    }

    private Object wrapIntoOptional(Object object){
        Optional optional = Optional.ofNullable(object);
        return optional;
    }

    private Collection<Object> getChildObjectsByPrimariesList(Class childClass, List<?> primariesList){
        ConfigHandler configHandler = new ConfigHandler();
        List<Object> childObjectList = (List<Object>) configHandler.handle(childClass);
        List<Object> childObjectListToReturn = new ArrayList<>();
        for(Object object: childObjectList){
            Field field = findFieldWithPrimary(object);
            if(field == null){
                System.out.println("[openYaml] Cannot find primary key in class " + object.getClass().getName());
                return null;
            }
            Object valueOfField = getValueFromField(object, field);
            if(valueOfField == null){
                System.out.println("[openYaml] Cannot get value from field " + field.getName() + " of class " + object.getClass().getName());
                return null;
            }
            if(primariesList.contains(valueOfField)){
                childObjectListToReturn.add(object);
            }
        }
        if(childObjectListToReturn.isEmpty()){
            return null;
        }else{
            return childObjectListToReturn;
        }
    }

    private Object getChildObjectByPrimary(Class childClass, Object primary){
        ConfigHandler configHandler = new ConfigHandler();
        List<Object> childObjectList = (List<Object>) configHandler.handle(childClass);
        for(Object object: childObjectList){
            Field field = findFieldWithPrimary(object);
            if(field == null){
                System.out.println("[openYaml] Cannot find primary key in class " + object.getClass().getName());
                return null;
            }
            Object valueOfField = getValueFromField(object, field);
            if(valueOfField == null){
                System.out.println("[openYaml] Cannot get value from field " + field.getName() + " of class " + object.getClass().getName());
                return null;
            }
            if(primary.equals(valueOfField)){
                return object;
            }
        }
        return null;
    }

    private Class defineRootClass(Field field){
        Class currentClassToRoot = field.getType();
        if(Optional.class.isAssignableFrom(currentClassToRoot)){
            ParameterizedType parameterizedType = (ParameterizedType) field.getGenericType();
            Type type = parameterizedType.getActualTypeArguments()[0];
            Class aClass;
            if(type instanceof ParameterizedTypeImpl){
                ParameterizedTypeImpl parameterizedTypeImpl = (ParameterizedTypeImpl) type;
                aClass = parameterizedTypeImpl.getRawType();
                String string = parameterizedTypeImpl.getTypeName();
                Pattern pattern = Pattern.compile("<.+>");
                Matcher matcher = pattern.matcher(string);
                if(matcher.find()){
                    String matched = matcher.group();
                    matched = matched.replaceAll("<", "").replaceAll(">", "");
                    if(matched.contains(",")){
                        return Map.class;
                    }
                    try {
                        return Class.forName(matched);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }else{
                aClass = (Class) type;
            }
            currentClassToRoot = aClass;
        }
        if(Collection.class.isAssignableFrom(currentClassToRoot)){
            ParameterizedType parameterizedType = (ParameterizedType) field.getGenericType();
            currentClassToRoot = (Class<?>) parameterizedType.getActualTypeArguments()[0];
        }
        return currentClassToRoot;
    }

    private boolean isCollectionTypeField(Field field){
        Class fieldClass = field.getType();
        if(Optional.class.isAssignableFrom(fieldClass)){
            ParameterizedType parameterizedType = (ParameterizedType) field.getGenericType();
            Type type = parameterizedType.getActualTypeArguments()[0];
            if(type instanceof ParameterizedTypeImpl){
                fieldClass = ((ParameterizedTypeImpl) type).getRawType();
            } else {
                fieldClass = (Class) type;
            }

        }
        return Collection.class.isAssignableFrom(fieldClass);
    }

    private void fillFieldWithValue(Object configObject, Field fieldToFill, Object valueToFill){
        fieldToFill.setAccessible(true);
        try {
            fieldToFill.set(configObject, valueToFill);
        } catch (IllegalAccessException e) {
            System.out.println("[openYaml] Cannot access(set) to field " + fieldToFill.getName() + " in config class " + configObject.getClass().getName() + ".");
        }
        fieldToFill.setAccessible(false);
    }

    private Object getValueFromField(Object object, Field field){
        Object valueOfField = null;
        try {
            field.setAccessible(true);
            valueOfField = field.get(object);
        } catch (IllegalAccessException e) {
            field.setAccessible(false);
        }
        field.setAccessible(false);
        return valueOfField;
    }

    public Object getPath(Object configObject){
        List<Field> fieldList = getFields(configObject);
        Field fieldNeeded = null;
        for(Field field: fieldList){
            if(annotationHandler.hasPathAnnotation(field)){
                fieldNeeded = field;
                break;
            }
        }
        if(fieldNeeded != null){
            Object object = null;
            fieldNeeded.setAccessible(true);
            try {
                object = fieldNeeded.get(configObject);
            } catch (IllegalAccessException e) {
                System.out.println("[openYaml] Cannot access @ConfigPath field!");
                fieldNeeded.setAccessible(false);
                return null;
            }
            fieldNeeded.setAccessible(false);
            return object;
        }
        return null;
    }

    private Field findFieldWithPrimary(Object customObject){
        List<Field> fieldList = getFields(customObject);
        for(Field field: fieldList){
            List<Annotation> annotationList = annotationHandler.getAnnotations(field);
            if(annotationHandler.getAnnotationFromList(annotationList, Primary.class).isPresent()){
                return field;
            }
        }
        return null;
    }
}
