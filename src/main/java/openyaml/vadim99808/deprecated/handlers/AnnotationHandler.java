package openyaml.vadim99808.deprecated.handlers;

import openyaml.vadim99808.deprecated.annotations.ConfigPath;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class AnnotationHandler {

    public List<Annotation> getAnnotations(Field field){
        Annotation[] annotations = field.getAnnotations();
        return new ArrayList<>(Arrays.asList(annotations));
    }

    public Optional<Annotation> getAnnotationFromList(List<Annotation> annotationList, Class neededAnnotation){
        Optional<Annotation> optionalAnnotation = Optional.empty();
        for(Annotation annotation: annotationList){
            if(annotation.annotationType().equals(neededAnnotation)){
                optionalAnnotation = Optional.of(annotation);
            }
        }
        return optionalAnnotation;
    }

    public boolean hasPathAnnotation(Field field){
        Annotation[] annotations = field.getAnnotations();
        return Arrays.stream(annotations).anyMatch(annotation -> annotation instanceof ConfigPath);
    }

}
