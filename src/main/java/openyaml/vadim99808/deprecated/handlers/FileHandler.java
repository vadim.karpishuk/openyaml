package openyaml.vadim99808.deprecated.handlers;

import java.io.File;

public class FileHandler {

    private FieldHandler fieldHandler;

    public FileHandler(){
        fieldHandler = new FieldHandler();
    }

    public File getPath(Object objectConfiguration){
        Object object = fieldHandler.getPath(objectConfiguration);
        if(object != null){
            return (File) object;
        }
        return null;
    }
}
